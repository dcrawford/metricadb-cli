#!/usr/bin/env python

PROJECT = 'metricadb'

# Change docs/sphinx/conf.py too!
VERSION = '0.1'

# Bootstrap installation of Distribute
import distribute_setup
distribute_setup.use_setuptools()

from setuptools import setup, find_packages

try:
    long_description = open('README.rst', 'rt').read()
except IOError:
    long_description = ''


# requires = ['distribute', 'cliff', 'requests', 'psycopg2', 'pyodbc',
#             'mysql-connector-python']
requires = ['prettytable==0.6.1', 'distribute', 'cliff', 'requests']

setup(
    name=PROJECT,
    version=VERSION,

    description='Metrica analytics',
    long_description=long_description,

    author='David Crawford',
    author_email='david@getmetrica.com',

    classifiers=['Development Status :: 3 - Alpha',
                 'License :: OSI Approved :: Apache Software License',
                 'Programming Language :: Python',
                 'Programming Language :: Python :: 2',
                 'Programming Language :: Python :: 2.7',
                 'Intended Audience :: Developers',
                 'Environment :: Console',
                 ],

    platforms=['Any'],

    scripts=[],

    provides=[],
    install_requires=requires,

    namespace_packages=[],
    packages=find_packages(),
    include_package_data=True,

    entry_points={
        'console_scripts': [
            'metrica = metrica.cli:main'
        ],
        'metrica': [
            'create = metrica.commands.create:Create',
            'connect = metrica.commands.connect:Connect',
            'start = metrica.commands.start:Start',
            'load = metrica.commands.load:Load',
            'install = metrica.commands.install:Install',
        ],
    },

    zip_safe=False,
)
