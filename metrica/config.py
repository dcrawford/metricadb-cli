import json
import logging
import os


LOG = logging.getLogger(__name__)


class ConfigManager(object):
    def __init__(self, app_label):
        self.app_label = app_label
        self.values = {}
        self.path = self._get_config_paths()[0]

    def load(self):
        paths = self._get_config_paths()
        for path in paths:
            try:
                with open(path) as conf_file:
                    config_json = json.loads(conf_file.read())
                self.path = path
                self.values = config_json
                LOG.debug("Loaded config from %s" % path)
                break
            except Exception as e:
                LOG.debug("No valid config file at %s\n%s" % (path, e))

    def _get_config_paths(self):
        paths = (
            os.path.join('~', '.%s.conf' % self.app_label),
            os.path.join('~', '.%s' % self.app_label, 'config')
        )
        return map(os.path.expanduser, paths)

    def save(self):
        LOG.debug("Saving config to %s" % self.path)
        with open(self.path, 'w') as conf_file:
            conf_file.write(json.dumps(self.values))
