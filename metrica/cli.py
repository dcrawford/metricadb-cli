import logging
import os
import sys
import traceback

from cliff.app import App
from cliff.commandmanager import CommandManager
from metrica.config import ConfigManager


LOG = logging.getLogger(__name__)
LOG_PATH = os.path.expanduser(os.path.join('~', '.metrica.log'))
LABEL = 'metrica'


class CLI(App):
    def __init__(self):
        super(CLI, self).__init__(
            description='Metrica Analytics',
            version='0.1',
            command_manager=CommandManager(LABEL)
        )
        self.parser.set_defaults(log_file=LOG_PATH)

    def initialize_app(self, *args, **kws):
        super(CLI, self).initialize_app(*args, **kws)
        self.config = ConfigManager(LABEL)
        self.config.load()

        requests_log = logging.getLogger("requests")
        requests_log.setLevel(logging.ERROR)
        requests_log.propagate = True

    def clean_up(self, *args, **kws):
        self.config.save()
        if 'err' in kws:
            trace = '\n'.join(traceback.format_exception(*sys.exc_info()))
            LOG.warn(trace)
        super(CLI, self).clean_up(*args, **kws)


def main(argv=sys.argv[1:]):
    return CLI().run(argv)

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
