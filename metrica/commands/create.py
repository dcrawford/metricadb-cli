import argparse
import requests

from cliff.command import Command
from datetime import date
from metrica.constants import API_URL
from metrica.models import project


class Create(Command):
    "Create a new metrica project"

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(
            description=self.get_description(),
            prog=prog_name
        )
        parser.add_argument('name', metavar='NAME', type=str,
                            help='The name of the new project.')
        return parser

    def take_action(self, parsed_args):
        new_project = project.create(date.today(), parsed_args.name)
        auth = self.app.config.values.get('auth', None)
        if not auth:
            raise Exception(
                "Please run 'metrica start' first to login.")
        email = auth['email']
        api_key = auth['api_key']
        r = requests.post(API_URL + 'database/', auth=(email, api_key))
        if r.status_code is 401:
            raise Exception(
                "Looks like your username or password has changed. " +
                "Please run 'metrica start' again to update them.")

        try:
            new_project.config['database_id'] = r.json()['id']
        except:
            raise Exception(
                "Could not create a new metricadb instance for this " +
                "project.  Please try again soon.")

        new_project.save()
        self.app.stdout.write("Created project %s.\n" % new_project.name)
