import argparse
import logging
import metrica.models.database as metricadb

from cliff.command import Command
from metrica.types import database_uri


LOG = logging.getLogger(__name__)


class Connect(Command):
    "Connect to a new database"

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(
            description=self.get_description(),
            prog=prog_name
        )
        parser.add_argument('--uri', metavar='URI', type=database_uri,
                            help="A full URI to your database, e.g. " +
                            "mysql://user:pass@localhost:3333/exampledb.")
        parser.add_argument('--type', help="The type of database server. " +
                            "Supported options are psql or mysql.")
        parser.add_argument('--host', help='Hostname')
        parser.add_argument('--port', help='Port', type=int)
        parser.add_argument('--user', help='Username')
        parser.add_argument('--password', help='Password')
        parser.add_argument('--database',
                            help='Name of the database to connect to.')
        parser.add_argument('--list', action='store_true',
                            help='List connected databases.')
        return parser

    def take_action(self, parsed_args):
        if parsed_args.list:
            self.list_connections()
            return

        if parsed_args.uri:
            connect_info = parsed_args.uri
        else:
            missing_arg_msg = ("Please specify either --uri or --type, " +
                               "--host, --port, --user, --password, and " +
                               "--database")
            if (not parsed_args.type or not parsed_args.host or
                    not parsed_args.port or not parsed_args.user or
                    not parsed_args.password or not parsed_args.database):
                self.get_parser('metrica').error(missing_arg_msg)

            connect_info = parsed_args

        self.assert_connection(connect_info)

        databases = self.app.config.values.get('databases', {})
        count = 1
        name = connect_info['type']
        while name in databases:
            count += 1
            name = connect_info['type'] + str(count)
        databases[name] = connect_info
        self.app.config.values['databases'] = databases

        self.app.stdout.write(("Connected!  Now you can load data with " +
                               "metrica load %s <dest_table_name>.\n") % name)

    def assert_connection(self, connect_info):
        try:
            db = metricadb.get(connect_info)
            db.connect()
            db.close()
        except Exception as e:
            LOG.debug("Could not connect to database:\n%s\n%s" %
                     (connect_info, e))
            raise Exception("Sorry, there was trouble connecting to the " +
                            "database.  Please check the information you " +
                            "provided and try again.  Error:\n%s" % e)

    def list_connections(self):
        databases = self.app.config.values.get('databases', [])
        if len(databases) is 0:
            self.app.stdout.write("No databases connected.\n")
            return

        self.app.stdout.write("Connected databases:\n")
        for name, db in databases.iteritems():
            self.app.stdout.write(
                "\t%s - %s://%s:%s@%s:%s/%s\n" %
                (name, db['type'], db['user'], db['password'], db['host'],
                db['port'], db['database']))
