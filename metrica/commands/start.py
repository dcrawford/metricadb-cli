import logging
import requests

from getpass import getpass
from cliff.command import Command
from metrica.constants import API_URL


LOG = logging.getLogger(__name__)


class Start(Command):
    "Initialize Metrica."

    def take_action(self, parsed_args):
        email = raw_input("Metrica email: ")
        password = getpass("Metrica password: ")

        r = requests.get(API_URL + 'current_user/', auth=(email, password))
        if r.status_code == 401:
            raise Exception(
                "Could not authenticate.  Please try again.\n")
        if r.status_code is not 200:
            raise Exception(
                "Sorry, there was a problem reaching the server. " +
                "Please try again soon.")
        try:
            if hasattr(r.json, '__call__'):
                api_key = r.json()['api_key']
            else:
                api_key = r.json['api_key']
        except:
            raise Exception(
                "Sorry, there was a problem reaching the server. " +
                "Please try again soon.")

        self.app.config.values['auth'] = {
            'email': email,
            'api_key': api_key
        }

        self.app.stdout.write("You're logged in!  To get started, create a " +
                              "new project with\n\n" +
                              "\tmetrica create project_name\n\n")
