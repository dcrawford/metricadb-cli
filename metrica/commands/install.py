import argparse
import logging

from cliff.command import Command
from distutils.spawn import find_executable, spawn, DistutilsExecError


LOG = logging.getLogger(__name__)


class Install(Command):
    "Install connectors for different databases."

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(
            description=self.get_description(),
            prog=prog_name
        )
        parser.add_argument('database', metavar='DATABASE',
                            choices=['postgresql', 'mysql', 'vertica'],
                            help="The database connector you want to install. "
                            "Options are: postgresql, mysql, vertica.")
        return parser

    def take_action(self, parsed_args):
        if hasattr(self, 'check_%s' % parsed_args.database):
            if getattr(self, 'check_%s' % parsed_args.database)():
                self.app.stdout.write("Connector for %s already installed.\n" %
                                      parsed_args.database)
                return
        getattr(self, 'install_%s' % parsed_args.database)()

    def check_mysql(self):
        try:
            import mysql.connector
            return True
        except ImportError:
            return False

    def check_postgresql(self):
        try:
            import psycopg2
            return True
        except ImportError:
            return False

    def check_vertica(self):
        try:
            import pyodbc
            return True
        except ImportError:
            return False

    def install_mysql(self):
        import pip
        pip.main(['install', 'mysql-connector-python'])

    def install_postgresql(self):
        import pip
        if find_executable('yum'):
            try:
                spawn(['yum', 'install', 'postgresql-libs', 'python-devel'])
                pip.main(['install', 'pyscopg2'])
            except DistutilsExecError:
                raise Exception("Could not install dependencies for postgres.")
        elif find_executable('apt-get'):
            try:
                spawn(['sudo', 'apt-get', 'install', 'python-psycopg2'])
            except DistutilsExecError:
                raise Exception("Could not install dependencies for postgres.")
        else:
            pip.main(['install', 'psycopg2'])

    def install_vertica(self):
        import pip
        if find_executable('yum'):
            try:
                spawn(['yum', 'install', 'unixodbc'])
                pip.main(['install', 'pyodbc'])
            except DistutilsExecError:
                raise Exception("Could not install dependencies for vertica.")
        elif find_executable('apt-get'):
            try:
                spawn(['sudo', 'apt-get', 'install', 'unixodbc'])
                pip.main(['install', 'pyodbc'])
            except DistutilsExecError:
                raise Exception("Could not install dependencies for vertica.")
