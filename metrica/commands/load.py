import argparse
import json
import logging
import requests
import zlib
import metrica.models.database as metricadb

from StringIO import StringIO
from datetime import datetime
from cliff.command import Command
from metrica.constants import API_URL
from metrica.models import project


LOG = logging.getLogger(__name__)


class Load(Command):
    "Load data from a database into the current project."

    def get_parser(self, prog_name):
        parser = argparse.ArgumentParser(
            description=self.get_description(),
            prog=prog_name
        )
        parser.add_argument('--append', action='store_true',
                            help="Append data to table if it already exists.")
        parser.add_argument('--overwrite', action='store_true',
                            help="Overwrite table if it already exists.")
        parser.add_argument('source', metavar='SOURCE',
                            help="The source database name (created with " +
                            "'metrica connect').")
        parser.add_argument('table', metavar='TABLE',
                            help="The name of the table to load data into. " +
                                 "It will be created if it does not exist.")

        return parser

    def get_query(self):
        sql = ''
        while ';' not in sql:
            sql += raw_input('> ')
        return sql

    def take_action(self, parsed_args):
        current_project = project.load()
        databases = self.app.config.values['databases']
        if parsed_args.source not in databases:
            raise Exception(("Source '%s' does not exist.  Use 'metrica " +
                             "connect --list' to see your connected sources " +
                             "or 'metrica connect' to add a new one.") %
                            parsed_args.source)
        source = databases[parsed_args.source]
        sql = self.get_query()

        dest_db_id = current_project.config['database_id']

        db = metricadb.get(source)
        db.connect()
        try:
            result = db.query(sql)
            table_url = API_URL + 'database/%s/table/%s' % (dest_db_id,
                                                            parsed_args.table)
            columns = [{'name': c[0],
                        'type': db.get_postgres_column_type(c)}
                       for c in result.description]
            table_dict = {
                'name': parsed_args.table,
                'database_id': dest_db_id,
                'columns': columns
            }
            auth = self.app.config.values['auth']
            httpauth = (auth['email'], auth['api_key'])

            if parsed_args.overwrite:
                r = requests.delete(table_url, auth=httpauth)
                if r.status_code >= 300:
                    raise Exception("There was a problem overwriting the " +
                                    "table.  Please try again later.")

            r = requests.put(table_url,
                             data=json.dumps(table_dict),
                             headers={'Content-Type': 'application/json'},
                             auth=httpauth)
            if r.status_code == 409:
                if not parsed_args.overwrite and not parsed_args.append:
                    raise Exception(("The table '%s' already exists.  Please" +
                                     " use --append or --overwrite.") %
                                    parsed_args.table)
            elif r.status_code >= 300:
                raise Exception("There was an error creating the table.")

            start = datetime.now()
            self.app.stdout.write("Loading data into metricadb.%s... " %
                                  parsed_args.table)
            self.app.stdout.flush()
            total_rows = 0
            total_zipped_bytes = 0
            for rowset in result:
                data = StringIO()
                gzip = zlib.compressobj()
                for row in rowset:
                    data.write(gzip.compress('\t'.join((r'\N' if c is None
                                             else str(c) for c in row))))
                    data.write(gzip.compress('\n'))
                data.write(gzip.flush())
                total_rows += len(rowset)
                total_zipped_bytes += data.len
                r = requests.post(table_url + '/row/',
                                  data=data.getvalue(),
                                  headers={'Content-Type': 'text/tsv',
                                           'Content-Encoding': 'gzip'},
                                  auth=httpauth)
                if r.status_code >= 300:
                    result.close()
                    raise Exception("There was an error loading data.")
            elapsed = int((datetime.now() - start).total_seconds())
            self.app.stdout.write("%d rows loaded in %d seconds!\n" %
                                  (total_rows, elapsed))
            self.app.stdout.write(("\nVisit http://www.metricadb.com/%s to " +
                                   "start querying your data.\n\n") %
                                  dest_db_id)
            meta = {
                'date_completed': datetime.now(),
                'elapsed_seconds': elapsed,
                'rows_loaded': total_rows,
                'zipped_bytes_loaded': total_zipped_bytes
            }
            current_project.add_load_script(parsed_args.source,
                                            parsed_args.table,
                                            sql,
                                            meta)
        except KeyboardInterrupt:
            self.app.stdout.write('\nLoad cancelled.\n')
            LOG.debug("User canceled load.")
        finally:
            db.close()
