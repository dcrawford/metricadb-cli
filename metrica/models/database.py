from datetime import date, time, datetime


def pick(d, keys):
    return dict(filter(lambda item: item[0] in keys, d.iteritems()))


def get(connect_info):
    if connect_info['type'] == 'postgres':
        return Postgres(connect_info)
    elif connect_info['type'] == 'mysql':
        return Mysql(connect_info)
    elif connect_info['type'] == 'vertica':
        return Vertica(connect_info)
    else:
        raise Exception("Unrecognized database type")


class Database(object):
    def __init__(self, connect_info):
        self.connect_info = pick(connect_info, ['host', 'port', 'user',
                                                'password', 'database'])

    def close(self):
        self.connection.close()

    class Result(object):
        def __init__(self, cursor):
            self.cursor = cursor
            self.description = cursor.description

        def __iter__(self):
            return self

        def next(self):
            rowset = self.cursor.fetchmany(100000)
            if len(rowset) is 0:
                self.cursor.close()
                raise StopIteration
            return rowset

        def close(self):
            self.cursor.close()

    def query(self, sql):
        if not self.connection:
            raise Exception("You must connect the database before querying.")
        cursor = self.connection.cursor()
        cursor.execute(sql)
        return Database.Result(cursor)


POSTGRES_TYPES = {
    16: 'bool',
    18: 'char',
    20: 'int8',
    21: 'int2',
    22: 'int2vector',
    23: 'int4',
    25: 'text',
    700: 'float4',
    701: 'float8',
    1042: 'bpchar',
    1043: 'varchar',
    1082: 'date',
    1083: 'time',
    1114: 'timestamp',
    1184: 'timestamptz',
    1266: 'timetz',
    1700: 'numeric'
}
DEFAULT_TYPE = 'varchar'
SIZED_TYPES = [18, 1042, 1043]
PRECISIONED_TYPES = [1700]


class Postgres(Database):
    def connect(self):
        try:
            import psycopg2
        except ImportError:
            raise Exception("Postgresql is not installed.  Please run 'sudo "
                            "metrica install postgresql'.")
        self.connection = psycopg2.connect(**self.connect_info)

    def get_postgres_column_type(self, column_definition):
        type_code = column_definition[1]
        internal_size = column_definition[3]
        precision = column_definition[4]
        scale = column_definition[5]
        type = POSTGRES_TYPES.get(type_code, DEFAULT_TYPE)
        if type_code in PRECISIONED_TYPES:
            type += "(%s,%s)" % (precision, scale)
        elif type_code in SIZED_TYPES and internal_size:
            type += "(%s)" % internal_size
        return type


MYSQL_TYPES = {
    'BIT': 'bit',
    'BLOB': 'text',
    'DATE': 'date',
    'DATETIME': 'timestamp',
    'DECIMAL': 'numeric',
    'DOUBLE': 'double precision',
    'FLOAT': 'float8',
    'INT24': 'int8',
    'NUMERIC': 'numeric',
    'LONG': 'int8',
    'TIME': 'time',
    'TIMESTAMP': 'timestamp',
    'TINY': 'int2',
}


class Mysql(Database):
    def connect(self):
        try:
            import mysql.connector
        except ImportError:
            raise Exception("MySQL is not installed.  Please run 'sudo "
                            "metrica install mysql'.")
        for key in ['host', 'user', 'password', 'database']:
            self.connect_info[key] = str(self.connect_info[key])
        self.connect_info['port'] = int(self.connect_info['port'])
        self.connection = mysql.connector.connect(**self.connect_info)

    def get_postgres_column_type(self, column_definition):
        import mysql.connector
        type = mysql.connector.FieldType.get_info(column_definition[1])
        pg_type = MYSQL_TYPES.get(type, DEFAULT_TYPE)
        return pg_type


ODBC_TYPES = {
    str: 'varchar',
    unicode: 'varchar',
    int: 'int8',
    float: 'float8',
    datetime: 'timestamp',
    date: 'date',
    time: 'time'
}


class Vertica(Database):
    def connect(self):
        try:
            import pyodbc
        except ImportError:
            raise Exception("Vertica is not installed.  Please run 'sudo "
                            "metrica install vertica'.")
        try:
            self.connection = pyodbc.connect(
                "DRIVER={VerticaSQL};SERVER=%(host)s;PORT=%(port)d;"
                "DATABASE=%(database)s;UID=%(user)s;"
                "PWD=%(password)s" %
                self.connect_info, ansi=True)
        except Exception as e:
            if e[0] == '00000' and 'image not found' in e[1]:
                raise Exception("Connecting to a Vertica database requires "
                                "installation of the Vertica ODBC driver. "
                                "It can be downloaded from the Vertica "
                                "website.")
            raise

    def get_postgres_column_type(self, column_definition):
        type = column_definition[1]
        pg_type = ODBC_TYPES.get(type, DEFAULT_TYPE)
        return pg_type
