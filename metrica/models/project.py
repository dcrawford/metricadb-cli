import json
import logging
import os

from datetime import date
from metrica.utils import ensure_dir


LOG = logging.getLogger(__name__)
RELATIVE_CONFIG_DIR_PATH = '.metrica'
RELATIVE_CONFIG_FILE_PATH = os.path.join(RELATIVE_CONFIG_DIR_PATH, 'config')
RELATIVE_LOADS_DIR_PATH = 'loads'


def load():
    path = os.getcwd()
    while path and path != '/':
        try:
            config_path = os.path.join(path, RELATIVE_CONFIG_FILE_PATH)
            with open(config_path) as project_file:
                project_json = json.loads(project_file.read())
            return Project(path, project_json)
        except Exception as e:
            LOG.debug("No project config file at %s\n%s" % (path, e))
        path, _ = os.path.split(path)
    raise Exception("No project found. This command must be run inside a " +
                    "project directory created with 'metrica create'.")


def create(created_date, name):
    d = date.today()
    date_string = "%04d%02d%02d" % (d.year, d.month, d.day)
    dir_name = date_string + '-' + name
    dir_path = os.path.join(os.getcwd(), dir_name)
    try:
        os.mkdir(dir_path, 0755)
        os.mkdir(os.path.join(dir_path, RELATIVE_CONFIG_DIR_PATH), 0755)
        os.mkdir(os.path.join(dir_path, RELATIVE_LOADS_DIR_PATH), 0755)
    except OSError as e:
        raise Exception("Couldn't create the project directory '%s'.\n%s" %
                        (dir_path, e))

    project = Project(dir_path, name=name, date=date_string)
    project.save()
    return project


class Project(object):
    def __init__(self, path, config_dict=None, **config):
        self.config = config_dict or config
        self.path = path
        if 'name' not in self.config or 'date' not in self.config:
            raise Exception(("Invalid project configuration in %s\n" +
                             "No project name or date found.") % self.path)
        self.name = self.config['name']
        self.date = self.config['date']

    def save(self):
        config_path = os.path.join(self.path, RELATIVE_CONFIG_FILE_PATH)
        with open(config_path, 'w') as config_file:
            config_file.write(json.dumps(self.config))

    def add_load_script(self, source, table, sql, meta):
        loads_dir_path = os.path.join(self.path, RELATIVE_LOADS_DIR_PATH)
        ensure_dir(loads_dir_path)
        load_script_name = "%s.%s.sql" % (table, source)
        load_script_path = os.path.join(loads_dir_path, load_script_name)
        with open(load_script_path, 'a') as load_file:
            for key in meta:
                load_file.write("-- %s: %s\n" % (key, meta[key]))
            load_file.write(sql + '\n\n')
