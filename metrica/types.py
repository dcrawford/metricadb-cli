from argparse import ArgumentTypeError
from urlparse import urlparse


SCHEMES = {
    'mysql': 'mysql',
    'postgresql': 'postgres',
    'postgres': 'postgres',
    'pgsql': 'postgres',
    'vertica': 'vertica',
    'vsql': 'vertica',
}


def database_uri(uri_string):
    if uri_string[:5] == "jdbc:":
        uri_string = uri_string[5:]
    parsed_uri = urlparse(uri_string)
    if not parsed_uri.scheme or parsed_uri.scheme not in SCHEMES:
        raise ArgumentTypeError("URI does not include a valid scheme. "
                                "Valid schemes are %s.  Example valid URI:"
                                "\n\tmysql://user:pass@host:3333/dbname" %
                                ', '.join(SCHEMES.keys()))
    if not parsed_uri.hostname:
        raise ArgumentTypeError("URI does not include a valid hostname. "
                                "Example valid URI:"
                                "\n\tmysql://user:pass@host:3333/dbname")
    try:
        if not parsed_uri.port:
            raise ArgumentTypeError("URI does not include a valid port. "
                                    "Example valid URI:"
                                    "\n\tmysql://user:pass@host:3333/dbname")
    except ValueError:
        raise ArgumentTypeError("URI does not include a valid port. "
                                "Example valid URI:"
                                "\n\tmysql://user:pass@host:3333/dbname")
    if not parsed_uri.username:
        raise ArgumentTypeError("URI does not include a valid username. "
                                "Example valid URI:"
                                "\n\tmysql://user:pass@host:3333/dbname")
    if not parsed_uri.password:
        raise ArgumentTypeError("URI does not include a valid password. "
                                "Example valid URI:"
                                "\n\tmysql://user:pass@host:3333/dbname")
    if not parsed_uri.path or parsed_uri.path == '/':
        raise ArgumentTypeError("URI does not include a valid database name."
                                " Example valid URI:"
                                "\n\tmysql://user:pass@host:3333/dbname")

    return {
        'type': SCHEMES[parsed_uri.scheme],
        'host': parsed_uri.hostname,
        'port': parsed_uri.port,
        'user': parsed_uri.username,
        'password': parsed_uri.password,
        'database': parsed_uri.path[1:]
    }
